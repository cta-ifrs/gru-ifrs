<?php 
  /*
    Copyright (C) 2017  CTA - Centro Tecnológico de Acessibilidade

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

/*
  Sistema para emissão de GRU do IFRS - Bento Gonçalves

  @author Lael
  @author Lucas
*/
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <!-- Metadata -->
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">

  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="css/styles.css?t=<?php echo time() ?>" media="screen" />
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-theme.min.css" media="screen" />

  <!-- JS -->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery.maskedinput.min.js"></script>
  <!-- <script src="js/jquery.mask.min.js"></script> -->
  <script type="text/javascript" src="js/jquery.maskMoney.min.js"></script>
  <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/gru.money.js?t=<?php echo time() ?>"></script>
  <script type="text/javascript" src="js/behaviour.js?t=<?php echo time() ?>"></script>

  <title>Sistema para emissão de GRU</title>
</head>

<body>

<?php
  date_default_timezone_set("America/Sao_Paulo");
  setlocale(LC_MONETARY,"pt_BR");

  require_once('Loader/SimpleAutoloader.php');
  SimpleAutoloader::requireAll('.');
  SimpleAutoloader::requireAll('Picqer/Barcode');


  function now(){
    $now = $_SERVER['REQUEST_TIME_FLOAT'];
    $now = str_replace(".", "", strval($now));
    return $now;
  }

  // Códigos de recolhimento utilizados pelo IFRS
  // Obs: Informados pelo requisitante do sistema.
  $arrayServicos = array(
      // 'Ticket alimentação' => '28837-3',
      // 'Serviço de hospedagem e alimentação' => '_28837-3',
      // 'Receita da produção vegetal' => '28811-0',
      // 'Receita da produção animal e derivados' => '28812-8',
      // 'Outras receitas' => '28848-9'
      'Serviços de hospedagem e alimentação (Ticket)' => '28837-3',
      'Outras multas (Biblioteca)' => '28848-9'
  );

  /**
      TABELA DE VALORES

      Obs: Valores devem ser escritos no padrão R$:
           * 0,50
           * 10,38
           * 1.345,00

  */
  $arrayValorTicket = array(
    'servidor' => new GRU\Money('10,00'),
    'aluno'    => new GRU\Money('5,00')
  );


  if ($_SERVER['REQUEST_METHOD'] === 'POST'){

    $validator = new GRU\GRUFormValidator($_POST);

    if ($validator->validate()) {

        $valorTotal = null;

        if ($_POST['tipoServico'] == '28837-3') { //&& !isset($_POST['valorTotal'])) {
          $qtde = $_POST['quantidade'];
          $soa = $_POST['soa'];
          $valor = $arrayValorTicket[$soa]->mul($qtde);
          $valorTotal = strval($valor);
          unset($_POST['valorTotal']);
        } else {
          $valorTotal = $_POST['valorTotal'];

        }

        $gruBuilder = new GRU\GRUBuilder();
        $gru = null;
        
        $tipoServico = preg_replace('/_/', '', $_POST['tipoServico']);

        // Dados do IFRS - Retirados das GRUs de amostra utilizadas no desenvolvimento 
        // do sistema
        $gruBuilder->setUnidadeGestora('INST.FED.DO RS/CAMPUS BENTO GONCALVES');
        $gruBuilder->setUnidadeGestoraId('158264');
        $gruBuilder->setGestaoId('26419');

        $data = new DateTime();
        $data->modify('+3 days');
        $gruBuilder->setCompetencia($data->format('m/Y'));
        $gruBuilder->setVencimento($data->format('d/m/Y'));
        $gruBuilder->setValorTotal($valorTotal);
        
        if ($_POST['tipoServico'] == '28848-9') {
          $gruBuilder->setMulta($valorTotal);
          $gruBuilder->setValorPrincipal('0,00');
        }

        $gruBuilder->setRecolhimentoId($tipoServico);

        // GRUs geradas possuem a referência com 555 no início.
        $gruBuilder->setReferenciaId('555' . now());
        $gruBuilder->setNome($_POST['nome']);
        $gruBuilder->setContribuinte($_POST['contribuinte']);

        // Identificação do BB
        $gruBuilder->setOrganizationId('0001');

        // Código do convênio BB - Definido no manual do Tesouro Nacional para 
        // GRU (Leiaute Segmento 9, Campo 7).
        $gruBuilder->setConvenio('01095523');

        $gru = $gruBuilder->build();

        include "_gru.view";

        echo '<br/><br/><img src="img/cut.png">';
        echo str_repeat('-', 132);
        echo '</p><br/><br/>';

        include "_gru.view";

        include "_pdfbtn.form";

    // Dados incorretos
    } else {
        $erros = $validator->getErrors();
        $valoresAntigos = $_POST;
        include "_gru.form";      
    }

  // method = GET  
  } else {

      include "_gru.form";

  }
?>

</body>
</html>