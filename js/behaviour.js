/*
    Copyright (C) 2017  CTA - Centro Tecnológico de Acessibilidade

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

$(function($) {
  var valueAluno = $("#alunoprice").val();
  var valueServidor = $("#servidorprice").val();

  var tickets = {
    "servidor": new GRUMoney(valueServidor),
    "aluno": new GRUMoney(valueAluno)
  }
  
  function calcTicket(type, qty) {
    return tickets[type].mul(qty).toString();
  } 

  function showPf() {
    $("#contribuinte").mask("999.999.999-99");
    $("#contribuinte").prop("placeholder", "Insira o CPF do contribuinte");
    $(".contribuinte .label-text").text("CPF do contribuinte:");
    // $('.cnpj').addClass("powerhidden");
    // $('.cnpj #contribuinte').removeAttr('required');
    // $('.cpf').removeClass("powerhidden");
    // $('.cpf #contribuinte').attr('required','');
  }

  function showPj() {
    $("#contribuinte").mask("99.999.999/9999-99");
    $("#contribuinte").prop("placeholder", "Insira o CNPJ do contribuinte");
    $(".contribuinte .label-text").text("CNPJ do contribuinte:");
    // $('.cpf').addClass("powerhidden");
    // $('.cpf #contribuinte').removeAttr('required');
    // $('.cnpj').removeClass("powerhidden");
    // $('.cnpj #contribuinte').attr('required','');
  }

  $("#vencimento").mask("99/99/9999");
  // $(".cpf #contribuinte").mask("999.999.999-99");
  // $(".cnpj #contribuinte").mask("99.999.999/9999-99");
  $("#referenciaId").mask("9?999999999");
  $("#valorTotal").maskMoney({'decimal': ',', 'thousands': '.'});
  
  $('.valorTotal #valorTotal').removeAttr('required');
  $('.valorTotal #valorTotal').attr('placeholder', '0,00');
  $('.valorTotal #valorTotal').attr('disabled', '');

  $('#tipoServico').change(function(){
    switch($('#tipoServico').val()){
      case '28848-9':
      case '28812-8':
      case '28811-0':
      case '_28837-3':
        $('.quantidade').addClass("powerhidden")
        $('.quantidade #quantidade').removeAttr('required')
        $('.quantidade #quantidade').val('');
        $('.soa').addClass("powerhidden")
        $('.valorTotal #valorTotal').prop('disabled', false)
        //$('.valorTotal #valorTotal').prop('value', '')
        break;
      case '28837-3':
        if ($('select option:selected').val() == '28837-3') {
          $('.quantidade').removeClass("powerhidden")
          $('.quantidade #quantidade').attr('required','')
          $('.soa').removeClass("powerhidden")
          $('.valorTotal #valorTotal').attr('disabled', '')
          if($('#servidor').is(':checked')){
            $('.valorTotal #valorTotal').prop('value', calcTicket('servidor', $('#quantidade').val()))
          }
          if($('#aluno').is(':checked')){
            $('.valorTotal #valorTotal').prop('value', calcTicket('aluno', $('#quantidade').val()))
          }
        }else{
          $('.quantidade').addClass("powerhidden")
          $('.quantidade #quantidade').removeAttr('required')
          $("#quantidade").val("");
          $('.soa').addClass("powerhidden")
          $('.valorTotal #valorTotal').prop('disabled', false)
          //$('.valorTotal #valorTotal').prop('value', '')
        };
        break;
    }
    
  });

  $($('#quantidade')).change(function(){
    if($('#servidor').is(':checked')){
      $('.valorTotal #valorTotal').prop('value', calcTicket('servidor', $('#quantidade').val()))
    }
    if($('#aluno').is(':checked')){
      $('.valorTotal #valorTotal').prop('value', calcTicket('aluno', $('#quantidade').val()))
    }
    $("#valorTotal").maskMoney('mask');
  });

  $($('input:radio[name="soa"]')).change(function(){
    if($('#servidor').is(':checked') && $('#quantidade').val()!=null){
      $('.valorTotal #valorTotal').prop('value', calcTicket('servidor', $('#quantidade').val()));
    }
    if($('#aluno').is(':checked') && $('#quantidade').val()!=null){
      $('.valorTotal #valorTotal').prop('value', calcTicket('aluno', $('#quantidade').val()));
    }
    $("#valorTotal").maskMoney('mask');
  });

  $('#pf').click(function(){
    showPf();
    $("span.error").remove();
  });

  $("#pj").click(function(){
    showPj();
    $("span.error").remove();
  });

  $('#gru_form').submit(function(event){
    $("#errormgr").val("false");
    if ($('#pf').is(':checked')) {
      $('.cnpj #contribuinte').prop("disabled", true);
    } else {
      $('.cpf #contribuinte').prop("disabled", true);
    }
  });

  if ($("#pf").prop("checked")) {
    showPf();
  } else {
    showPj();
  }

  $("#tipoServico").trigger("change");

  if ($("#errormgr").val() == "false") {
    $("span.error").remove();
  }
  if ($("#servidor").is(':checked') || $("#aluno").is(':checked')) {
    $("div.soa span.error").remove();   
  }

  $("#valorTotal").maskMoney('mask');

  $(".label-text-extra").remove();


});
