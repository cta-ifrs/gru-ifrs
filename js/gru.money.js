/*
    Copyright (C) 2017  CTA - Centro Tecnológico de Acessibilidade

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

**/


function GRUMoney(strValue){
    var type = typeof strValue;
    if (type === "number") {
        this._cent = strValue;

    } else if (type === "string") {
        var regexValues = /(\d+),(\d{2})/;
        strValue = strValue.replace(/\./, "");
        
        var match = regexValues.exec(strValue);    
        var real = match[1];
        var cent = match[2];

        this._cent = parseInt(real, 10) * 100 + parseInt(cent, 10);
    
    } else {
        // error
    }

    return this;
}

GRUMoney.prototype.mul = function(value) {
    var newcent = this._cent * value;
    return new GRUMoney(newcent);
}

GRUMoney.prototype.toString = function() {
    var cent = this._cent % 100;
    var real = ((this._cent - cent) / 100) | 0;

    real = real.toString();
    var strEnd = real.length;
    var strReal = '';

    for (var i = (strEnd - 1), dotCount = 0; 
         i >= 0; 
         i--, dotCount++) 
    {
        if (dotCount !== 0 && dotCount % 3 == 0) {
            strReal = real[i] + '.' + strReal;
        } else {
            strReal = real[i] + strReal;
        }
    }

    var centZero = cent < 10 ? '0' : '';

    return strReal + ',' + centZero + cent;
}



