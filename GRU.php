<?php

/*
    Copyright (C) 2017  CTA - Centro Tecnológico de Acessibilidade

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


/*
 * Definição da classe que representa a GRU - Guia de Recolhimento da União
 *
 * @author Lucas
*/

  namespace GRU;

  /**
  * Exception lançada quando a propriedade acessada
  * não existe.
  */
  class InvalidProperty extends \Exception{
  }

  /**
  * Exception lançada quando o método acessado
  * não existe.
  */
  class InvalidMethod extends \Exception{
  }

  /**
  * Exception lançada quando o formato do valor não está
  * de acordo com a validação.
  */
  class ValueInvalidFormat extends \Exception{
  }

  /**
  * Classe que representa a GRU.
  * 
  */
  class GRU {

      private $_data = array(
        'nome' => '',
        'unidadeGestora' => '',
        'recolhimentoId' => '',
        'referenciaId' => '',
        'competencia' => '',
        'vencimento' => '',
        'contribuinte' => '',
        'unidadeGestoraId' => '',
        'gestaoId' => '',
        'valorPrincipal' => '',
        'valorTotal' => '',
        'multa' => ''
      );

      private $_codigoDeBarra;
      private $_dac;

      private $_validate = array(
          'nome' => '/^[a-zA-Z_\s\-\x{00c0}-\x{00ff}]+$/u',
          'unidadeGestora' => '/^[a-zA-Z]|\.|\s+$/',
          'recolhimentoId' => '/^\d{5}\-\d$/',
          //'referenciaId' => '/^\d{1,16}$/',
          'referenciaId' => '/^\d{1,20}$/',
          'competencia' => '/^\d{2}\/\d{4}$/',
          'vencimento' => '/^\d{2}\/\d{2}\/\d{4}$/',
          'contribuinte' => '/^(\d{3}\.\d{3}\.\d{3}\-\d{2})|(\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2})$/',
          'unidadeGestoraId' => '/^\d{6}$/',
          'gestaoId' => '/^\d{5}$/',
          'valorPrincipal' => '/(^\d{1,3}\,\d{2}$)|(^\d{1,3}\.\d{3}\,\d{2}$)|(^\d{1,3}\.\d{3}\.\d{3}\,\d{2}$)|(^\d\.(\d{3}\.){2}\d{3}\,\d{2}$)/',
          'valorTotal' =>     '/(^\d{1,3}\,\d{2}$)|(^\d{1,3}\.\d{3}\,\d{2}$)|(^\d{1,3}\.\d{3}\.\d{3}\,\d{2}$)|(^\d\.(\d{3}\.){2}\d{3}\,\d{2}$)/',
          'multa' =>     '/(^\d{1,3}\,\d{2}$)|(^\d{1,3}\.\d{3}\,\d{2}$)|(^\d{1,3}\.\d{3}\.\d{3}\,\d{2}$)|(^\d\.(\d{3}\.){2}\d{3}\,\d{2}$)/',
          
          //'valorPrincipal' => '/^\d+(\.\d{1,2}){0,1}$/',
          //'valorTotal' => '/^\d+(\.\d{1,2}){0,1}$/'
          // 'codigoDeBarra' => '/\d{44}/',
          // 'codigoDeBarraFmt' => '/(\d{11}\-\d){4}/'
      );

      private function _processAttribute($name, $value = null){
        $vars = $this->_data;
        $propertyName = $name;
        if (array_key_exists($propertyName, $vars)){

            if ($value !== null){
              if (1 === preg_match($this->_validate[$name], $value)){
                $this->$propertyName = $value;
              } else {
                  throw new ValueInvalidFormat("Atributo {$name} / {$value}
                                                está com valor inválido");
              }
            } else {
                return $this->$propertyName;
            }
        } else {
          throw new InvalidProperty("Atributo {$name} não existe");
        }
      }

      public function __set($name, $value){
        $this->_processAttribute($name, $value);
      }

      public function __get($name) {
        return $this->_processAttribute($name);
      }

      public function __call($name, array $args) {
        $matches = array();
        $countArgs = count($args);
        if (preg_match('/^(set|get)(\w+)$/', $name, $matches)) {
          $property = lcfirst($matches[2]);
          $methodType = $matches[1];
          if ($methodType === 'set') {
            $this->$property = $args[0];
          } else {
            return $this->$property;
          }

          return true;
        }

        $stringArgs = implode(',',$args);
        throw new InvalidMethod("Método inválido {$name}({$stringArgs})");

      }

      public function setCodigoDeBarra($codigoDeBarra){
        // TODO: Talvez uma validação (Regexp ?)
        $this->_codigoDeBarra = $codigoDeBarra;
      }

      public function getCodigoDeBarra(){
        return $this->_codigoDeBarra;
      }

      public function setDAC(array $dac){
        $this->_dac = $dac;
      }

      public function getCodigoDeBarraCompleto(){
        $cdbFormatado = '';

        for ($i = 0; $i < 4; $i++){
          $block = substr($this->_codigoDeBarra, $i*11, 11);
          $dac = $this->_dac[$i];
          $cdbFormatado .= sprintf('%s-%d', $block, $dac);
          if ($i < 3) $cdbFormatado .= ' ';
        }

        return $cdbFormatado;
      }
  }
