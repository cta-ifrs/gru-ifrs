<?php
    
/*
    Copyright (C) 2017  CTA - Centro Tecnológico de Acessibilidade

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

  namespace GRU;

  class GRUFormValidator {
    private $_data;
    private $_errors = array();

    public function __construct(array $arrayData) {
        $this->_data = $arrayData;
    }

    public function validate() {
        $data = $this->_data;

        if (isset($data['nome'])) {
            if (!preg_match('/^[a-zA-Z_\s\-\x{00c0}-\x{00ff}]+$/u', $data['nome'])) {
                $this->_errors['nome'] = "Campo inválido, verifique o nome informado";
                return false;    
            }
        } else {
            $this->_errors['nome'] = "Informe o nome do contribuinte";
            return false;
        }

        if (isset($data['contribuinte'])) {
            
            $contribuinte = preg_replace('/\.|\/|-/', '', $data['contribuinte']);
            if ($data['tipo'] == 'pf') {
                $formatado = preg_match('/^(\d{3}\.\d{3}\.\d{3}\-\d{2})$/', $data['contribuinte']);
                if (!$formatado || !$this->isValidCPF($contribuinte)) {
                    $this->_errors['contribuinte'] = "CPF inválido";
                    return false;                
                }
            } else {
                $formatado = preg_match('/^(\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2})$/', $data['contribuinte']);
                if (!$formatado || !$this->isValidCNPJ($contribuinte)) {
                    $this->_errors['contribuinte'] = "CNPJ inválido";
                    return false;                
                }
            }
            
        } else {
            $this->_errors['contribuinte'] = "Informe o CPF/CNPJ do contribuinte";
            return false;       
        }

        $data['tipoServico'] = preg_replace('/_/', '', $data['tipoServico']);

        if ($data['tipoServico'] == '28837-3' && (!isset($data['soa']) || $data['soa'] == '') ) {
            $this->_errors['soa'] = "Selecione uma opção";
            return false;          
        }

        if ($data['tipoServico'] == '28837-3' && !isset($data['valorTotal'])) { // Ticket alimentação 
            if (isset($data['quantidade'])) {
                if (!preg_match('/^[0-9]+$/', $data['quantidade'])) {
                    $this->_errors['quantidade'] = "Informe um valor válido";
                    return false;           
                }

                if (intval($data['quantidade']) <= 0 || intval($data['quantidade']) > 10000) {
                    $this->_errors['quantidade'] = "Informe um valor válido entre 1 e 10000";
                    return false;           
                }

            } else {
                $this->_errors['quantidade'] = "Informe a quantidade de tickets desejados";
                return false;       
            }
        } else if (isset($data['valorTotal'])) {
            if (!preg_match('/(^\d{1,3}\,\d{2}$)|(^\d{1,3}\.\d{3}\,\d{2}$)|(^\d{1,3}\.\d{3}\.\d{3}\,\d{2}$)|(^\d\.(\d{3}\.){2}\d{3}\,\d{2}$)/', $data['valorTotal']))
            { 
                $this->_errors['valorTotal'] = "Informe um valor válido";
                return false;           
            }

        } else {
            $this->_errors['valorTotal'] = "Informe o valor da GRU";
            return false;       
        }

        return true;
    }

    public function getErrors() {
        return $this->_errors;
    }


    public function isValidCPF($cpf) {
        /*
         * This function is part of Respect/Validation.
         *
         * https://github.com/Respect/Validation/blob/master/library/Rules/Cpf.php
         */

        $c = preg_replace('/\D/', '', $cpf);
        if (mb_strlen($c) != 11 || preg_match("/^{$c[0]}{11}$/", $c)) {
            return false;
        }
        for ($s = 10, $n = 0, $i = 0; $s >= 2; $n += $c[$i++] * $s--);
        if ($c[9] != ((($n %= 11) < 2) ? 0 : 11 - $n)) {
            return false;
        }
        for ($s = 11, $n = 0, $i = 0; $s >= 2; $n += $c[$i++] * $s--);
        if ($c[10] != ((($n %= 11) < 2) ? 0 : 11 - $n)) {
            return false;
        }
        return true;
    }

    public function isValidCNPJ($cnpj) {
        /*
         * This function is part of Respect/Validation.
         *
         * https://github.com/Respect/Validation/blob/master/library/Rules/Cnpj.php
         */

        if (!is_scalar($cnpj)) {
            return false;
        }
        
        $cleanInput = preg_replace('/\D/', '', $cnpj);
        $b = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
        if ($cleanInput < 1) {
            return false;
        }
        if (mb_strlen($cleanInput) != 14) {
            return false;
        }
        for ($i = 0, $n = 0; $i < 12; $n += $cleanInput[$i] * $b[++$i]);
        if ($cleanInput[12] != ((($n %= 11) < 2) ? 0 : 11 - $n)) {
            return false;
        }
        for ($i = 0, $n = 0; $i <= 12; $n += $cleanInput[$i] * $b[$i++]);
        if ($cleanInput[13] != ((($n %= 11) < 2) ? 0 : 11 - $n)) {
            return false;
        }
        return true;
    }
    


  }