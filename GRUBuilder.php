<?php

/*
    Copyright (C) 2017  CTA - Centro Tecnológico de Acessibilidade

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


/*
 * Definição da classe que irá gerar a GRU. Esta classe é responsável por
 * gerar o código de barras (Segmento 9) e montar um objeto GRU contendo todos 
 * os campos necessários.
 * A GRU gerada segue os padrões utilizados pelo campus do IFRS - Bento Gonçalves.
 *
 * @author Lucas
*/

  namespace GRU;

  class GRUBuilder {
      static $GESTAO_NICKNAME = array(
        '26419' => '10474'
      );

      private $_constGRUData;

      private $_minutes;
      private $_seconds;
      private $_orgId;
      private $_convenio;

      private $_gru;

      public function __construct(){
        $this->_gru = new GRU();
        $this->_constGRUData = array();
        $this->_minutes = $this->_seconds = null;
        $this->_orgId = null;
        $this->_convenio = null;
      }

      // TODO Este método será removido
      public function setData(array $data){
        foreach ($data as $field => $value) {
          try {
            $this->_gru->$field = $value;
          } catch (InvalidProperty $e) {
            // Neste caso, ignora propriedades inválidas
          }
        }
      }

      private function _makeVerifyingDigit($numbers){
        $vDigit = 0;
        $m = 2;

        foreach (str_split(strrev($numbers)) as $value){
          $vDigit += $m * intval($value);
          if ((++$m) > 9) $m = 2;
        }

        // DAC módulo 11
        $vDigit = $vDigit % 11;

        if ($vDigit <= 1){
          return '0';
        } else if ($vDigit == 10) {
          return '1';
        }

        return strval(11 - $vDigit);
      }

      public function setMinutesAndSeconds($minutes, $seconds){
        $m = intval($minutes);
        $s = intval($seconds);
        if ($m < 0 || $m > 59 || $s < 0 || $s > 59){
            throw new \Exception('Horário inválido');
        }

        $this->_minutes = $m;
        $this->_seconds = $s;
      }

      public function setSegment($seg){
          throw new \Exception('No momento aceita somente segmento 9');
      }

      public function setValueId($id){
        throw new \Exception('No momento aceita somente identificador de valor 9');
      }

      public function setOrganizationId($identificationCode) {
        $id = strval($identificationCode);
        if (!preg_match('/\d{4}/', $id)) {
          throw new \Exception('Identificador deve possuir 4 dígitos');
        }

        $this->_orgId = $id;
      }

      public function setConvenio($convenio) {
        if (!preg_match('/^\d{8}$/', $convenio)){
          throw new \Exception('Convênio inválido');
        }

        $this->_convenio = $convenio;
      }

      /**
      * Cria o código de barras baseado no layout de segmento 9 (GRU Simples)
      */
      private function _makeBarCode(){
        $barcode = null;

        $barcode = '8';        // Id da arrecadção
        $barcode .= '9';       // Banco - uso interno
        $barcode .= '9';       // Id do valor
        //$barcode .= 'D';       // Digito verificador

        $valorTotal = $this->_gru->valorTotal;
        $valorTotal = preg_replace('/\,|\./', '', $valorTotal);

        $barcode .= sprintf('%011s', $valorTotal);  // Valor (11 casas)

        if ($this->_orgId == null){
          throw new \Exception('Identificador da organização não informado');
        }

        if ($this->_convenio == null){
          throw new \Exception('Convênio não informado');
        }

        $barcode .= $this->_orgId; //'0001';     // Identificação BB
        $barcode .= $this->_convenio;  //'01095523'; // Convênio BB
        $barcode .= '16';       // Identifca os campos preenchidos

        // Código de recolhimento
        $barcode .= preg_replace('/\-\d/', '', $this->_gru->recolhimentoId);

        // Apelido da UG/Gestão
        $barcode .= GRUBuilder::$GESTAO_NICKNAME[$this->_gru->gestaoId];

        // CPF ou CNPJ
        $regCPF = '/^\d{3}\.\d{3}\.\d{3}\-\d{2}$/';
        $regCNPJ = '/^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/';
        if (preg_match($regCPF, $this->_gru->contribuinte)){
          $barcode .= '1';
        } else if (preg_match($regCNPJ, $this->_gru->contribuinte)) {
          $barcode .= '2';
        } else {
          // Não deveria entrar aqui ja que a GRU possui filtro.. mas...
          throw new \Exception('CPF ou CNPJ do contribuinte é inválido');
        }

        // Minuto e segundo no momento que gerou a GRU
        if ($this->_minutes != null && $this->_seconds != null){
          $barcode .= strval($this->_minutes);
          $barcode .= strval($this->_seconds);
        } else {
          $barcode .= date('i');
          $barcode .= date('s');
        }

        $vDigit = $this->_makeVerifyingDigit($barcode);

        $barcode = substr_replace($barcode, $vDigit, 3, 0);

        $dac = array();
        for ($i = 0; $i < 4; $i++){
          $block = substr($barcode, $i*11, 11);
          $dac[] = $this->_makeVerifyingDigit($block);
        }

        $this->_gru->setCodigoDeBarra($barcode);
        $this->_gru->setDAC($dac);

        //var_dump($this->_gru->getCompleteBarCode());
      }

      public function build(){
        foreach ($this->_constGRUData as $f => $v){
          $this->_gru->$f = $v;
        }

        $this->_makeBarCode();
        return $this->_gru;
      }

      public function __set($name, $value){
        $this->_constGRUData[$name] = $value;
      }

      public function __call($name, array $args) {
        $this->_gru->__call($name, $args);
      }
  }
