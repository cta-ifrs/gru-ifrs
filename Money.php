<?php
/*
    Copyright (C) 2017  CTA - Centro Tecnológico de Acessibilidade

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

    namespace GRU;

    class Money {
        private $_cent;
        public function __construct($stringValue) {
            if (gettype($stringValue) == "integer") {
                $this->_cent = $stringValue;

            // TODO Validar formato 
            } else if (is_string($stringValue)) {
                $values = array();
                $stringValue = preg_replace('/\./', '', $stringValue);
                preg_match('/^(\d+),(\d{2})$/', $stringValue, $values);
                $real = $values[1];
                $cent = $values[2];
                $this->_cent = intval($real) * 100 + $cent;

            } else  {
                throw new Exception("Invalid value");
            }
        }
        
        public function mul($m) {
            $r = $this->_cent * $m; 
            return new Money($r);
        }

        // TODO Adicionar outras operações além da multiplicação

        public function __toString() {
            $cent = $this->_cent % 100;
            $real = intval(($this->_cent - $cent) / 100);
            $real = str_split('' . $real);

            $strEnd = count($real);
            $strReal = '';
            for ($i = ($strEnd - 1), $dotCount = 0; 
                 $i >= 0; 
                 $i--, $dotCount++) 
            {
                if ($dotCount !== 0 && $dotCount % 3 == 0) {
                    $strReal = $real[$i] . '.' . $strReal;
                } else {
                    $strReal = $real[$i] . $strReal;
                }
            }

            return $strReal . ',' . sprintf('%02d', $cent);
        }
    }


