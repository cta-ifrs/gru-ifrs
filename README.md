# Sistema web para geração de GRU #


As GRUs geradas são baseadas no layout de segmento 9 definido nos manuais do Tesouro Nacional e da FEBRABAN (Federação Brasileira de Bancos). O propósito deste projeto é gerar GRUs para o refeitório do IFRS - Campus Bento.


**Manuais utilizados:** 

Layout Padrão de Arrecadação/Recebimento (http://www.febraban.org.br/7Rof7SWg6qmyvwJcFwF7I0aSDf9jyV/sitefebraban/Codbar4-v28052004.pdf) 

Código de Barras da GRU Simples e Judicial (http://www.tesouro.fazenda.gov.br/documents/10180/248853/Codigo_Barras_GRU_Simples_e_Judicial.pdf)