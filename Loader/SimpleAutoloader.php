<?php

	class SimpleAutoloader {

		public static function requireAll($dir){

			$di = new DirectoryIterator($dir);
			foreach ($di as $item) {
				if ($item->isDir() && !$item->isLink() && !$item->isDot()){
					//requireAll($item->getPath());
				} else if (substr($item->getFileName(), -4) === '.php' ) {
					$itemPath = $dir . "/" . $item;
					//echo "Loading: \"{$itemname}\"\n";
					require_once($itemPath);
				}
			}
		}


	}
