<?php
/*
    Copyright (C) 2017  CTA - Centro Tecnológico de Acessibilidade

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

require_once "../Money.php";
  

  class GRUMoney extends PHPUnit_Framework_TestCase {

    public function testReturnValueFormat(){
        $values = array('1.234,56', '0001,00', '100,00', '999,99', '9.999.999,09', '0,00', '00,02');
        $expect = array('1.234,56', '1,00', '100,00', '999,99', '9.999.999,09', '0,00', '0,02');
        
        foreach ($values as $i => $v) {
            $m = new GRU\Money($v);
            $this->assertEquals($expect[$i], strval($m));
        }
        
    }

    public function testMultiplication() {    
        $value = '9.979,01';
        // Multiplicador => Resultado esperado ($value * mul) 
        $mul =  array(1 => '9.979,01', 
                      2 => '19.958,02', 
                      3 => '29.937,03', 
                      4 => '39.916,04', 
                      5 => '49.895,05', 
                      6 => '59.874,06', 
                      7 => '69.853,07', 
                      8 => '79.832,08', 
                      9 => '89.811,09', 
                      10 => '99.790,10', 
                      11 => '109.769,11', 
                      12 => '119.748,12', 
                      13 => '129.727,13', 
                      101 => '1.007.880,01', 
                      1000 => '9.979.010,00'
                );

        foreach ($mul as $v => $result) {
            $m = new GRU\Money($value);
            $this->assertEquals($result, strval($m->mul($v)));
        }
        
    }

  }