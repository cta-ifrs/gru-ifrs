<?php

/*
    Copyright (C) 2017  CTA - Centro Tecnológico de Acessibilidade

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

  require_once "../GRU.php";
  require_once "../GRUBuilder.php";

  class GRUBuilderTest extends PHPUnit_Framework_TestCase {
    public function testGRUBuilderSetData() {
      date_default_timezone_set("America/Sao_Paulo");

      $sampleData = array(
        'nome'=>'Pedro Capota',
        'unidadeGestora'=>'ERRADO',
        'recolhimentoId'=>'28869-1', 'referenciaId'=>'363',
        'competencia'=>'07/2016', 'vencimento'=>'07/07/2016',
        'contribuinte'=>'000.000.000-22', 'unidadeGestoraId'=>'158264',
        'gestaoId'=>'26419', 'valorPrincipal'=>'104,01',
        'valorTotal'=>'104,01'
      );
      $gruBuilder = new GRU\GRUBuilder();
      $gruBuilder->setOrganizationId('0001');
      $gruBuilder->setConvenio('01095523');
      $gruBuilder->setData($sampleData);
      $gruBuilder->setUnidadeGestora('INST.FED.DO RS/CAMPUS BENTO GONCALVES');
      $gru = $gruBuilder->build();

      // Parâmetro constante
      $this->assertEquals('INST.FED.DO RS/CAMPUS BENTO GONCALVES', $gru->unidadeGestora);

      $this->assertEquals($sampleData['nome'], $gru->nome);
      $this->assertEquals($sampleData['recolhimentoId'], $gru->recolhimentoId);
      $this->assertEquals($sampleData['referenciaId'], $gru->referenciaId);
    }

    public function testGRUBuilderBarCodeGeneration(){
      $sampleData = array(
        'nome'=>'Pedro Capota',
        'unidadeGestora'=>'INST.FED.DO RS/CAMPUS BENTO GONCALVES',
        'recolhimentoId'=>'28869-1', 'referenciaId'=>'363',
        'competencia'=>'07/2016', 'vencimento'=>'07/07/2016',
        'contribuinte'=>'000.000.000-22', 'unidadeGestoraId'=>'158264',
        'gestaoId'=>'26419', 'valorPrincipal'=>'104,01',
        'valorTotal'=>'104,01'
      );

      $sampleBarCodeResult = '89920000001040100010109552316288691047414357';

      $gruBuilder = new GRU\GRUBuilder();
      $gruBuilder->setOrganizationId('0001');
      $gruBuilder->setConvenio('01095523');
      $gruBuilder->setMinutesAndSeconds(43, 57);
      $gruBuilder->setData($sampleData);
      $gru = $gruBuilder->build();

      $this->assertEquals($sampleBarCodeResult, $gru->getCodigoDeBarra(),
          'Código de barras incorreto');

    }

  }
