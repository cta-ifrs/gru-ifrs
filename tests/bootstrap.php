<?php
	/*
    Copyright (C) 2017  CTA - Centro Tecnológico de Acessibilidade

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

	require('../Loader/SimpleAutoloader.php');

	$autoloadFunc = function($class){
		$path = str_replace('\\', '/', $class);
		require_once('../' . $path . '.php');
	};

	// spl_autoload_register($autoloadFunc);
	// 
	// SimpleAutoloader::requireAll('..');
	//
	// spl_autoload_unregister($autoloadFunc);
