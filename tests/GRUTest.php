<?php

/*
    Copyright (C) 2017  CTA - Centro Tecnológico de Acessibilidade

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

  require_once "../GRU.php";
  //use GRU;

  class GRUTest extends PHPUnit_Framework_TestCase {

    public function testGRUMagicalSetterGetter(){
        $sampleData = array(
          'Pedro Capota', 'INST.FED.DO RS/CAMPUS BENTO GONCALVES',
          '28869-1', '363', '07/2016', '07/06/2016', '000.000.000-22',
          '158264', '26419', '1.001,00', '1.001,00'
        );

        $gru = new GRU\GRU();
        $gru->nome = $sampleData[0];
        $gru->unidadeGestora = $sampleData[1];
        $gru->recolhimentoId = $sampleData[2];
        $gru->referenciaId = $sampleData[3];
        $gru->competencia = $sampleData[4];
        $gru->vencimento = $sampleData[5];
        $gru->contribuinte = $sampleData[6];
        $gru->unidadeGestoraId = $sampleData[7];
        $gru->gestaoId = $sampleData[8];
        $gru->valorPrincipal = $sampleData[9];
        $gru->valorTotal = $sampleData[10];

        $this->assertEquals($sampleData[0], $gru->nome);
        $this->assertEquals($sampleData[1], $gru->unidadeGestora);
        $this->assertEquals($sampleData[2], $gru->recolhimentoId);
        $this->assertEquals($sampleData[3], $gru->referenciaId);
        $this->assertEquals($sampleData[4], $gru->competencia);
        $this->assertEquals($sampleData[5], $gru->vencimento);
        $this->assertEquals($sampleData[6], $gru->contribuinte);
        $this->assertEquals($sampleData[7], $gru->unidadeGestoraId);
        $this->assertEquals($sampleData[8], $gru->gestaoId);
        $this->assertEquals($sampleData[9], $gru->valorPrincipal);
        $this->assertEquals($sampleData[10], $gru->valorTotal);

    }


    // TODO Montar mais testes para a validação dos campos

    // public function testGRUFieldValidation(){
    //     $this->setExpectedException(ValueInvalidFormat);
    //     $gru = new GRU();
    //
    //     $gru->valorPrincipal = '234234324109.90';
    // }

  }
