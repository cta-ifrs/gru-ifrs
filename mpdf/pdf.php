<?php

include("mpdf.php");

$mpdf=new mPDF('c'); 

$mpdf->SetDisplayMode('fullpage');


// LOAD a stylesheet
$stylesheet = file_get_contents('../css/print.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

// $mpdf->WriteHTML($_POST['texto']);
$mpdf->WriteHTML('

  <table border="1" id="table_gru_pdf">
    <tr>
      <td rowspan="4" class="col12">
        <img src="../img/brasao.jpeg" width="70px">
      </td>
      <td rowspan="4" class="col38">
        <h1>Minist&eacute;rio da Fazenda <br>Secretaria do Tesouro Nacional</h1></br>
        <h2>Guia de Recolhimento da Uni&atilde;o - GRU</h2>
      </td>
      <td class="col25">Código de Recolhimento</td>
      <td class="col25 right_bold">'.$_POST['recolhimentoId'].'</td>
    </tr>
    <tr>
      <td class="col25">Número de Referência</td>
      <td class="col25 right_bold">'.$_POST['referenciaId'].'</td>
    </tr>
    <tr>
      <td class="col25">Competência</td>
      <td class="col25 right_bold">'.$_POST['competencia'].'</td>
    </tr>
    <tr>
      <td class="col25">Vencimento</td>
      <td class="col25 right_bold">'.$_POST['vencimento'].'</td>
    </tr>
    <tr>
      <td colspan="2">
        Nome do Contribuinte / Recolhedor:
        <div><strong>&nbsp;&nbsp;&nbsp;'.$_POST['nome'].'</strong></div>
      </td>
      <td class="col25">CNPJ ou CPF do Contribuinte</td>
      <td class="col25 right_bold">'.$_POST['contribuinte'].'</td>
    </tr>
    <tr>
      <td colspan="2">
        Nome da Unidade Favorecida:
        <div><strong>&nbsp;&nbsp;&nbsp;'.$_POST['unidadeGestora'].'</strong></div>
      </td>
      <td class="col25">UG/Gestão</td>
      <td class="col25 right_bold">'.$_POST['unidadeGestoraId'].' / '.$_POST['gestaoId'].'</td>
    </tr>
    <tr>
      <td colspan="2" rowspan="3">Instruções: As informações inseridas nessa guia são de exclusiva responsabilidade do contribuinte que deverá, em caso de dúvida, consultar a Unidade Favorecida dos recursos.</td>
      <td class="col25">(=) Valor do Principal</td>
      <td class="col25 right_bold">'.$_POST['valorPrincipal'].'</td>
    </tr>
    <tr>
      <td class="col25">(-) Desconto/Abatimento</td>
      <td class="col25"></td>
    </tr>
    <tr>
      <td class="col25">(-) Outras Deduções</td>
      <td class="col25"></td>
    </tr>
    <tr>
      <td rowspan="4" colspan="2" class="center">
        <div>GRU Simples</div>
        <div>Pagamento exclusivo no Banco do Brasil S.A.</div>
      </td>
      <td class="col25">(+) Mora/Multa</td>
      <td class="col25">' . $_POST['multa'] . '</td>
    </tr>
    <tr>
      <td class="col25">(+) Juros/Encargos</td>
      <td class="col25"></td>
    </tr>
    <tr>
      <td class="col25">(+) Outros Acréscimos</td>
      <td class="col25"></td>
    </tr>
    <tr>
      <td class="col25">(=) Valor Total</td>
      <td class="col25 right_bold">'.$_POST['valorTotal'].'</td>
    </tr>
  </table>
  <div id="gru_barcode">
  <span>'.$_POST['CodigoDeBarraCompleto'].'</span>
  <br/>
  <br/>
  <img width="500" src="data:image/png;base64,'.$_POST['CodigoDeBarra'].'">
  </div>
  <br/><br/><img src="../img/cut.png">'.str_repeat('-', 132).'</p><br/><br/>'.'

  <table border="1" id="table_gru_pdf">
    <tr>
      <td rowspan="4" class="col12">
        <img src="../img/brasao.jpeg" width="70px">
      </td>
      <td rowspan="4" class="col38">
        <h1>Minist&eacute;rio da Fazenda <br>Secretaria do Tesouro Nacional</h1></br>
        <h2>Guia de Recolhimento da Uni&atilde;o - GRU</h2>
      </td>
      <td class="col25">Código de Recolhimento</td>
      <td class="col25 right_bold">'.$_POST['recolhimentoId'].'</td>
    </tr>
    <tr>
      <td class="col25">Número de Referência</td>
      <td class="col25 right_bold">'.$_POST['referenciaId'].'</td>
    </tr>
    <tr>
      <td class="col25">Competência</td>
      <td class="col25 right_bold">'.$_POST['competencia'].'</td>
    </tr>
    <tr>
      <td class="col25">Vencimento</td>
      <td class="col25 right_bold">'.$_POST['vencimento'].'</td>
    </tr>
    <tr>
      <td colspan="2">
        Nome do Contribuinte / Recolhedor:
        <div><strong>&nbsp;&nbsp;&nbsp;'.$_POST['nome'].'</strong></div>
      </td>
      <td class="col25">CNPJ ou CPF do Contribuinte</td>
      <td class="col25 right_bold">'.$_POST['contribuinte'].'</td>
    </tr>
    <tr>
      <td colspan="2">
        Nome da Unidade Favorecida:
        <div><strong>&nbsp;&nbsp;&nbsp;'.$_POST['unidadeGestora'].'</strong></div>
      </td>
      <td class="col25">UG/Gestão</td>
      <td class="col25 right_bold">'.$_POST['unidadeGestoraId'].' / '.$_POST['gestaoId'].'</td>
    </tr>
    <tr>
      <td colspan="2" rowspan="3">Instruções: As informações inseridas nessa guia são de exclusiva responsabilidade do contribuinte que deverá, em caso de dúvida, consultar a Unidade Favorecida dos recursos.</td>
      <td class="col25">(=) Valor do Principal</td>
      <td class="col25 right_bold">'.$_POST['valorPrincipal'].'</td>
    </tr>
    <tr>
      <td class="col25">(-) Desconto/Abatimento</td>
      <td class="col25"></td>
    </tr>
    <tr>
      <td class="col25">(-) Outras Deduções</td>
      <td class="col25"></td>
    </tr>
    <tr>
      <td rowspan="4" colspan="2" class="center">
        <div>GRU Simples</div>
        <div>Pagamento exclusivo no Banco do Brasil S.A.</div>
      </td>
      <td class="col25">(+) Mora/Multa</td>
      <td class="col25">' . $_POST['multa'] . '</td>
    </tr>
    <tr>
      <td class="col25">(+) Juros/Encargos</td>
      <td class="col25"></td>
    </tr>
    <tr>
      <td class="col25">(+) Outros Acréscimos</td>
      <td class="col25"></td>
    </tr>
    <tr>
      <td class="col25">(=) Valor Total</td>
      <td class="col25 right_bold">'.$_POST['valorTotal'].'</td>
    </tr>
  </table>
  <div id="gru_barcode">
  '.$_POST['CodigoDeBarraCompleto'].'
  <br/>
  <br/>
  <img width="500" src="data:image/png;base64,'.$_POST['CodigoDeBarra'].'">
  </div>
');

$mpdf->Output();

exit;


?>